
### usage
### csvfile = "./data/d.csv"
### geodata = rgdal::readOGR("./data/Local_Authority_Districts_(December_2021)_ENG_BUC.geojson")
### z = writeseries(csvfile, geodata, "./data/ww.geojson")
###
### then
###
### 1. make sure the two bits of javascript spat out are set to the right variable names
###    at the top of the allMetadata.js
### 2. make sure the `timeData` object further down has the right min and max values.
### 3. view the map and maybe adjust the colour palette settings
### 


writeseries <- function(csvfile, geodata, output){
    dd = data.table::fread(csvfile)
    dd$V1 = NULL # chop first column
    nt = length(unique(dd$time))
    times = unique(dd$time)
    ns = length(unique(dd$location))
    ts = 0:(nt-1)
    widenames = c(outer(ts, c("",".L",".U"), paste0)) # 0, 1, 2, 3, ..., 1.L, 2.L, 3.L, ... 1.U, 2.U, 3.U...
    reordered = c(outer(c("",".L",".U"), ts, function(a,b){paste0(b,a)})) # 1, 1.L, 1.U, 2, 2.L, 2.U...
    
    dc = data.table::dcast(dd, location~time, value.var=c("mean","lower","upper"))
    names(dc)[-1] = widenames
    dc[,widenames] = dc[,..reordered]
    names(dc)[-1] = reordered
    ## does not play nicely: lmm = merge(geodata, dc, by.x="LAD21CD",by.y="location")
    lmm = geodata
    lmm@data = merge(geodata, dc, by.x="LAD21CD",by.y="location")              
    rgdal::writeOGR(lmm, output, driver="GeoJSON", layer="ww", overwrite_layer=TRUE)
    layerName = rjson::toJSON(as.character(times))
    geojsonname = rjson::toJSON(as.character(ts))


    cat("/* this layer geojsonname */","\n",geojsonname,";\n")
    cat("/* this layer layerName */\n",layerName,";\n")
    
    return(list(data = lmm,
                layerName=layerName,
                geojsonname = geojsonname))
}


### TODO layerName must be unique across layers across maps
predictionLayer <- function(){
    data = list(
        filename="data/model.geojson",
        xlabs = c("2021-05-31","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27"),
        geojsonname = c("0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17"),    
        geojsongeom = "LAD21NM",
        geojsonExtraInfo = "LAD21CD",
        friendlyname = "Wastewater gc/l 2021",
        centerLat = 54,
        centerLng = 0.7718,
        zoom = 8,
        country = "England",
        area = "LTLA",

        Ints = 1,
        Inis = 1,
        Num = "undefined",
        Vals = c("#FFF7EC", "#FEE9CC", "#FDD8A7", "#FDC38C", "#FCA16C", "#F67B51", "#E7533A", "#CF2518", "#AD0000", "#7F0000")
        
    )
    data$layerName = paste0("LAD",data$xlabs)
    return(data)
}

wwlayer <- function(){
    data = list(
        radius = 10,
        centerLat = 54,
        centerLng = 0.7718,
        zoom = 8
    )
    return(data)
}


regionalLayer <- function(){
    data = list(
        filename="data/modelregions.geojson",
        xlabs = c("2021-05-31","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27"),
        geojsonname = c("0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17"),    
        geojsongeom = "Region",
        geojsonExtraInfo = "Region",
        friendlyname = "Regional gc/l",
        centerLat = 54,
        centerLng = 0.7718,
        zoom = 8,
        country = "England",
        area = "Region",

        Ints = 1,
        Inis = 1,
        Num = "undefined",
        Vals = c("#FFF7EC", "#FEE9CC", "#FDD8A7", "#FDC38C", "#FCA16C", "#F67B51", "#E7533A", "#CF2518", "#AD0000", "#7F0000")
        
    )
    data$layerName = paste0("Regional",data$xlabs)
    return(data)
}



rnderlayer <- function(data){
    s = jinjar::render(fs::path(template), !!!data)
    message("// output to ",output)
    cat(s, file=output)
    invisible(0)
}
