Making the wwatlas
==================

From the top-level Wastewater project folder:

Run `do_all()` given an optional path to the outputs.

This reads:

  CSV of ltla time-series predictions
  gpkg of LTLA bounds
  CSV of regional predictions
  gpkg of region boundaries
  CSV of england time-series predictions
  
  CSV of week dates and indexes
  
  csv of national data
  
  allMetadata-brew.js template
  
Then builds layer info, and creates geojson and allMetadata.js for the map.

BUT the geojson for the WWtW layer needs to be created from the weekly averages from the 
spreadsheet data:

 ww_sp = layer_wwgc("./Outputs/2022-05-09/ww.weekly.rds")
 st_write(ww_sp,"./Mapping/wwatlas/data/ww_data.geojson")
 
 
