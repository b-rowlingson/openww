
weekcsv = "./Outputs/2022-05-09/time_index.csv"

ltlacsv = "./Outputs/2022-05-09/LTLA_pred_from_Jun2021_to_Jan2022_allsites.csv"
ltla2017 = "./Data/LTLA/2017/Local_Authority_Districts__December_2017__Boundaries_GB_BSC.gpkg"

rgncsv = "./Outputs/2022-05-09/Region_pred_from_Jun2021_to_Jan2022_allsites.csv"
regions_gpkg = "./Data/Regions/infuse_rgn_2011_clipped_gen.gpkg"


lad17model = layerLAD17(ltlacsv, weekcsv, ltla2017,"modelled.geojson","./Mapping/wwatlas/")
rgn11model = layerRGN11(rgncsv, weekcsv, regions_gpkg, "regionmodel.geojson","./Mapping.wwatlas")

layers = list(lad17model, rgn11model)
brew("./Mapping/Templates/allMetadata-brew.js", output="./Mapping/wwatlas/js/allMetadata.js")

